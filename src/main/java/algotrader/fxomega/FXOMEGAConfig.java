package algotrader.fxomega;

import ch.algotrader.config.ConfigName;

/**
 * The strategy configuration class
 */
public class FXOMEGAConfig {

    private long accountId;
    private long securityId;
    private long orderQuantity;

    public FXOMEGAConfig(
        @ConfigName("accountId") final long accountId,
        @ConfigName("securityId") final long securityId,
        @ConfigName("orderQuantity") final long orderQuantity) {

        this.accountId = accountId;
        this.securityId = securityId;
        this.orderQuantity = orderQuantity;
    }

    public long getAccountId() {
        return accountId;
    }

    public long getSecurityId() {
        return securityId;
    }

    public long getOrderQuantity() {
        return orderQuantity;
    }

    @Override
    public String toString() {
        return "FXOMEGAConfig {" +
            "accountId=" + accountId +
            "securityId=" + securityId +
            "orderQuantity=" + orderQuantity +
        "}";
    }
}
