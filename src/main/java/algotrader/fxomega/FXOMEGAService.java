package algotrader.fxomega;

import static ch.algotrader.util.TA4JUtil.toTick;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.ta4j.core.Bar;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.ATRIndicator;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.DifferenceIndicator;
import org.ta4j.core.num.Num;

import algotrader.fxomega.FXOMEGAConfig;
import ch.algotrader.entity.marketData.BarVO;
import ch.algotrader.entity.trade.OrderStatusVO;
import ch.algotrader.entity.trade.StopOrderVO;
import ch.algotrader.entity.trade.StopOrderVOBuilder;
import ch.algotrader.enumeration.Side;
import ch.algotrader.enumeration.Status;
import ch.algotrader.service.ConfigAwareStrategyService;
import ch.algotrader.vo.LifecycleEventVO;


/**
 * The main service class of the FXOMEGA Strategy
 */
public class FXOMEGAService extends ConfigAwareStrategyService<FXOMEGAConfig> {

	private final long accountId = 100;
	private final long securityId = 25;
	private double deviation = 0;
	private double stopLoss = 4.0;
	private double r = 1.5;
	private double takeProfit = stopLoss * r;
	private final double pipsize = .0001;
	private final String defaultAdapterType = "IB_NATIVE";
	private boolean orderPending = false;
	private boolean onMarket = false;
	private boolean isLong = false;
	private boolean isShort = false;
	private boolean signalGenerated = false;
	private BigDecimal orderQuantity;
	
	private int numBounces = 0;
	private int noOfCandles = 30;
	private int timeout = 2;
	int t = this.timeout;

	private int aTRPeriod = 14;
	private int eMAFastPeriod = 50;
	private int eMASlowPeriod = 200;
	private int prevIndex;

	private ATRIndicator atr;
	private EMAIndicator emaFast, emaSlow;

	private double entry;
	private double sl, tp;
	private TimeSeries series;

	private Bar currentBar;
	private Bar prevBar;
	private BigDecimal equity = new BigDecimal(100000);
	private BigDecimal exit;
	private DifferenceIndicator emaLong;
	private DifferenceIndicator emaShort;
	private Num currentEMAShort, currentEMALong;
	private Double currentUpperBand, currentLowerBand;
	private double factor = 0.5;


    public FXOMEGAService() {

        setStrategyName("FXOMEGA");
    }
    
    
	@Override
	public void onInit(final LifecycleEventVO event) {
		this.t = this.timeout;

		this.series = new BaseTimeSeries();
		ClosePriceIndicator closePriceIndicator = new ClosePriceIndicator(this.series);
		
		this.eMAFastPeriod = (int)Double.parseDouble(System.getProperty("eMAFastPeriod"));
		System.out.println(this.eMAFastPeriod);
		this.eMASlowPeriod = (int)Double.parseDouble(System.getProperty("eMASlowPeriod"));
		System.out.println(this.eMASlowPeriod);
		this.factor = Double.parseDouble(System.getProperty("factor"));
		System.out.println(this.factor);
		
		this.atr = new ATRIndicator(series, aTRPeriod);
		this.emaFast = new EMAIndicator(closePriceIndicator, eMAFastPeriod);
		this.emaSlow = new EMAIndicator(closePriceIndicator, eMASlowPeriod);

		this.emaLong = new DifferenceIndicator(emaSlow, emaFast);
		this.emaShort = new DifferenceIndicator(emaFast, emaSlow);
	}
	

    @Override
    public void onStart(final LifecycleEventVO event) {

        getSubscriptionService().subscribeMarketDataEvent(getStrategyName(), this.securityId, defaultAdapterType);
    }

    
    @Override
    public void onBar(BarVO bar) {
    	this.series.addBar(toTick(bar));
		int i = this.series.getEndIndex();
		
		
		if (i > eMASlowPeriod) {
			this.currentBar = this.series.getBar(i);
			this.currentEMAShort = emaShort.getValue(i);
			
			this.stopLoss = atr.getValue(i).doubleValue() * this.factor * 10000;
			this.takeProfit = this.stopLoss * r;

			if (!this.onMarket && !this.orderPending) {
				if (!this.signalGenerated) {
					if (emaFast.getValue(i).isGreaterThan(emaSlow.getValue(i))) {
						this.isLong = true;
						currentUpperBand = emaFast.getValue(i).doubleValue() + atr.getValue(i).doubleValue() * this.factor;
						currentLowerBand = emaFast.getValue(i).doubleValue() - atr.getValue(i).doubleValue() * this.factor;
						if (currentLowerBand < (this.currentBar.getMinPrice().doubleValue()) && this.currentBar.getMinPrice().doubleValue() < (currentUpperBand) && numBounces == 0) {
							this.prevIndex = i;
							numBounces++;
							
						}
						if (currentLowerBand < (this.currentBar.getMinPrice().doubleValue()) && this.currentBar.getMinPrice().doubleValue() < (currentUpperBand) && numBounces > 0) {
							if ((this.series.getEndIndex() - prevIndex) <= noOfCandles ) {
								this.entry = currentBar.getClosePrice().doubleValue() - (deviation * this.pipsize);
								this.sl = this.entry - (this.stopLoss * this.pipsize);
								this.tp = this.entry + (this.takeProfit * this.pipsize);
								System.out.println("ATR : " + atr.getValue(i));
								System.out.println(
										"[SIGNAL] LONG Entry : " + this.entry + " SL : " + this.sl + " TP : " + this.tp);
								this.signalGenerated = true;
								numBounces = 0;
							} else {
								numBounces = 0;
							}	
							//numBounces++;
						}
						
					}
					
					if (emaFast.getValue(i).isLessThan(emaSlow.getValue(i))) {
						this.isLong = true;
						currentUpperBand = (emaFast.getValue(i).doubleValue() + atr.getValue(i).doubleValue() * this.factor);
						currentLowerBand = emaFast.getValue(i).doubleValue() - atr.getValue(i).doubleValue() * this.factor;
						if (currentLowerBand < (this.currentBar.getMaxPrice().doubleValue()) && this.currentBar.getMaxPrice().doubleValue() < (currentUpperBand) && numBounces > 0) {
							this.prevIndex = i;
							numBounces++;
						}
						if (currentLowerBand < (this.currentBar.getMaxPrice().doubleValue()) && this.currentBar.getMaxPrice().doubleValue() < (currentUpperBand) && numBounces > 0) {
							if ((this.series.getEndIndex() - prevIndex) <= noOfCandles ) {
								this.entry = currentBar.getClosePrice().doubleValue() - (deviation * this.pipsize);
								this.sl = this.entry - (this.stopLoss * this.pipsize);
								this.tp = this.entry + (this.takeProfit * this.pipsize);
								System.out.println("ATR : " + atr.getValue(i));
								System.out.println(
										"[SIGNAL] LONG Entry : " + this.entry + " SL : " + this.sl + " TP : " + this.tp);
								this.signalGenerated = true;
								numBounces = 0;
							} else {
								numBounces = 0;
							}	
							//numBounces++;
						}
						
					}
				} else {
					if (this.t > 1) {
						if (this.currentBar.getMinPrice().doubleValue() <= this.entry
								&& this.entry <= this.currentBar.getMaxPrice().doubleValue()) {
							this.t = this.timeout;
							System.out.println("Entering position at " + this.series.getBar(i).getEndTime()
									+ " at Entry:" + this.entry);
							System.out.println();
							if (this.isLong) {
								sendEntryOrder(Side.BUY);
							} else if (this.isShort) {
								sendEntryOrder(Side.SELL_SHORT);
							}
						} else {
							System.out.println("Entry condition not met. Checked " + (timeout - t + 1) + " times.");
							this.t--;
						}
					} else if (this.t == 1) {
						this.t = this.timeout;
						if (this.currentBar.getMinPrice().doubleValue() <= this.entry
								&& this.entry <= this.currentBar.getMaxPrice().doubleValue()) {
							System.out.println("Entering position at " + this.series.getBar(i).getEndTime()
									+ " at Entry:" + this.entry);
							if (this.isLong) {
								sendEntryOrder(Side.BUY);
							} else if (this.isShort) {
								sendEntryOrder(Side.SELL_SHORT);
							}
						} else {
							System.out.println("Entry condition not met. Checked " + timeout + " times.");
							System.out.println("Entry conditions not met. timing out.");
							this.signalGenerated = false;
							this.isLong = false;
							this.isShort = false;
						}
					}
				}
			} else if (this.onMarket && !this.orderPending) {
				boolean slHit = (this.sl <= bar.getHigh().doubleValue()) && (this.sl >= bar.getLow().doubleValue());
				boolean tpHit = this.tp <= bar.getHigh().doubleValue() && this.tp >= bar.getLow().doubleValue();
				
				if (slHit) {
					this.exit = new BigDecimal(sl);
				} else if (tpHit) {
					this.exit = new BigDecimal(tp);
				}

				if (slHit || tpHit) {
					if (isLong) {
						sendExitOrder(Side.SELL);
					} else if (isShort) {
						sendExitOrder(Side.BUY);
					}
				}
			}
		}

    }
    
    
    private void sendEntryOrder(Side side) {
		this.equity = getPortfolioService().getCashBalance(getStrategyName());
		this.orderQuantity = ((equity.multiply(new BigDecimal(.01))).divide(new BigDecimal(stopLoss * 10), 5,
				RoundingMode.HALF_UP)).multiply(new BigDecimal(100000));

		System.out.println("Entry balance :" + getPortfolioService().getCashBalance(getStrategyName()));
		StopOrderVO order = StopOrderVOBuilder.create()
				.setStop(new BigDecimal(this.entry).setScale(5, RoundingMode.HALF_UP))
				.setStrategyId(getStrategy().getId()).setSecurityId(securityId).setSide(side).setQuantity(orderQuantity)
				.setAccountId(accountId).build();
		this.orderPending = true;
		getOrderService().sendOrder(order);
		System.out.println("Entry");
	}

	public void sendExitOrder(Side side) {
		StopOrderVO orderTP = StopOrderVOBuilder.create().setStop(this.exit.setScale(5, RoundingMode.HALF_UP))
				.setStrategyId(getStrategy().getId()).setSecurityId(securityId).setSide(side).setQuantity(orderQuantity)
				.setAccountId(accountId).build();
		this.orderPending = true;
		getOrderService().sendOrder(orderTP);
	}

	public void onOrderStatus(OrderStatusVO orderStatus) {
		if (orderStatus.getStatus().equals(Status.EXECUTED)) {
			if (!this.onMarket) {
				this.onMarket = true;
				this.orderPending = false;
			} else {
				this.orderPending = false;
				this.onMarket = false;
				this.signalGenerated = false;
				this.isLong = false;
				this.isShort = false;
				System.out.println("Exit balance :" + getPortfolioService().getCashBalance(getStrategyName()));
			}
		}
	}


}
